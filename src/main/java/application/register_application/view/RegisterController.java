package application.register_application.view;

import application.register_application.model.User;
import application.register_application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Controller
@Validated
public class RegisterController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/register")
    public ModelAndView registerView() {
        return new ModelAndView("register");
    }

    @PostMapping("register")
    public String register(@Validated @NotBlank @Size(min = 3, max = 20) String firstName, @Validated @NotBlank @Size(min = 3, max = 20) String lastName,
                           @Validated @NotBlank @Size(min = 3, max = 30) String email, @Validated @NotBlank @Size(min = 3, max = 20) String password,
                           @Validated @NotBlank @Size(min = 3, max = 30) String password2, Model model) {

        if (checkEmail(email, model)) {
            return "register";
        }

        return createNewUser(firstName, lastName, email, password, password2, model);
    }

    private boolean checkEmail(String email, Model model) {
        if (userRepository.findByEmail(email) != null) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Email already exist!");
            return true;
        }
        return false;
    }

    private String createNewUser(String firstName, String lastName, String email, String password, String password2, Model model) {
        if (password.equals(password2)) {
            User user = new User(UUID.randomUUID().toString(), firstName, lastName, email, password, true, "user");
            userRepository.save(user);
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "register correct!");
            return "register";
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "password are different!");
            return "register";
        }
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict(Model model) {
        model.addAttribute("messageType", "danger");
        model.addAttribute("message", "Validation goes wrong -.-");
        return new ModelAndView("register");
    }
}
