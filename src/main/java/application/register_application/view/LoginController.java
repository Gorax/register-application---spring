package application.register_application.view;

import application.register_application.model.User;
import application.register_application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Controller
@Validated
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("login")
    public ModelAndView loginView() {
        return new ModelAndView("login");
    }

    @PostMapping("login")
    public String login(@Validated @NotBlank @Size(min = 3, max = 30) String email, @Validated @NotBlank @Size(min = 3, max = 20) String password, HttpServletRequest req, Model model) {

        return tryLogin(email, password, req, model);
    }

    private String tryLogin(String email, String password, HttpServletRequest req, Model model) {
        User user = userRepository.findByEmail(email);

        if (user != null && user.getPassword().equals(password)) {
            req.getSession().setAttribute("id", user.getId());
            return "redirect:home";
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Invalid data!");
            return "login";
        }
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict(Model model) {
        model.addAttribute("messageType", "danger");
        model.addAttribute("message", "Invalid data");
        return new ModelAndView("login");
    }
}
