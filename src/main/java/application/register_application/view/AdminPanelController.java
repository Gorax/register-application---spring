package application.register_application.view;

import application.register_application.model.User;
import application.register_application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AdminPanelController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("userList")
    public ModelAndView userList(Model model, HttpServletRequest req) {
        if (authorization(req)) {
            model.addAttribute("userList", userRepository.findAll());
            return new ModelAndView("userList");
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "sneaky tricky, don't try that again");
            return new ModelAndView("login");
        }
    }

    private boolean authorization(HttpServletRequest req) {
        String id = (String) req.getSession().getAttribute("id");
        if (userRepository.findById(id).isPresent()) {
            User user = userRepository.findById(id).get();
            if ("admin".equals(user.getRole())) {
                return true;
            }
        }
        return false;
    }
}
