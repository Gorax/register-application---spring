package application.register_application.view;

import application.register_application.model.User;
import application.register_application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("home")
    public ModelAndView homeView(Model model, HttpServletRequest req) {
        String id = (String) req.getSession().getAttribute("id");
        User user = null;

        if (userRepository.findById(id).isPresent()) {
            user = userRepository.findById(id).get();
        }

        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("lastName", user.getLastName());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("isActive", user.isActive());
        model.addAttribute("role", user.getRole());
        return new ModelAndView("home");
    }
}
