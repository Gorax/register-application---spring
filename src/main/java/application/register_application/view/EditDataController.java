package application.register_application.view;

import application.register_application.model.User;
import application.register_application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Controller
@Validated
public class EditDataController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/editData")
    public ModelAndView editView(HttpServletRequest req, Model model) {
        User user = getUser(req);
        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("lastName", user.getLastName());
        model.addAttribute("email", user.getEmail());

        if (req.getParameter("success") != null) {
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "Data was changed");
        } else if (req.getParameter("invalid") != null) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Passwords are different");
        }

        return new ModelAndView("editData");
    }

    @PostMapping("/editData")
    public String editData(@Validated @NotBlank @Size(min = 3, max = 20) String firstName, @Validated @NotBlank @Size(min = 3, max = 20) String lastName,
                           @Validated @NotBlank @Size(min = 3, max = 30) String email, @Validated @NotBlank @Size(min = 3, max = 20) String password,
                           @Validated @NotBlank @Size(min = 3, max = 20) String password2, Model model, HttpServletRequest req) {

        User user = getUser(req);
        if (user != null) {
            if (password.trim().equals(password2.trim())) {
                user.setFirstName(firstName.trim());
                user.setLastName(lastName.trim());
                user.setEmail(email.trim());
                user.setPassword(password.trim());
                userRepository.save(user);
                return "redirect:editData?success";
            } else {
                return "redirect:editData?invalid";
            }
        } else {
            return "home?notFound";
        }
    }

    private User getUser(HttpServletRequest req) {
        String id = (String) req.getSession().getAttribute("id");
        User user = null;

        if (userRepository.findById(id).isPresent()) {
            user = userRepository.findById(id).get();
        }
        return user;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public String conflict(Model model) {
        model.addAttribute("messageType", "danger");
        model.addAttribute("message", "Invalid data");
        return "editData";
    }
}
