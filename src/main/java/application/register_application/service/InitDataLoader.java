package application.register_application.service;

import application.register_application.model.User;
import application.register_application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Service
public class InitDataLoader {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void init() {
        userRepository.save(new User(UUID.randomUUID().toString(), "admin", "admin", "admin@admin", "admin", true, "admin"));
        userRepository.save(new User(UUID.randomUUID().toString(), "user", "user", "user@user", "user", false, "user"));
    }
}
